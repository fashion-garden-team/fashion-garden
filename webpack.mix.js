let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/landing-page/landing-page.sass', 'public/css')
    .sass('resources/assets/sass/login-page/login-page.sass', 'public/css')
    .sass('resources/assets/sass/usr/users/nav.sass', 'public/css')
    .sass('resources/assets/sass/usr/timeline/timeline-nav.sass', 'public/css')
    .sass('resources/assets/sass/payment/payment.sass', 'public/css')
    .sass('resources/assets/sass/advertising/ads-register.sass', 'public/css')
    .sass('resources/assets/sass/shop/shop-register.sass', 'public/css')
    .sass('node_modules/bulma/bulma.sass', 'public/css')
    .js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/ui/tablet-down-nav.js', 'public/js/ui')
    .js('node_modules/@fortawesome/fontawesome-free/js/all.min.js', 'public/js');
