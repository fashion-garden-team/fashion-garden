<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Class namespaces */
use App\Post;
use App\Shop;
use App\Item;
/* Basic routing */

Route::get('/', function () {
    return view('pub.pages.single.landing-page');
})->name('home');
Route::get('/login', function () {
    return view('pub.pages.account.login-page');
})->name('login');
Route::get('/about', function () {
    return view('pub.pages.single.about-page');
})->name('about');
Route::get('/terms-and-conditions', function () {
    return view('pub.pages.single.terms-and-conditions-page');
});

/* Admin routing */


/* User routing */
Route::get('/up', function () {
    return view('usr.profile.profile');
})
->name('user-profile');

Route::get('/timeline', function () {
    $data = Post::info();
    return view('usr.timeline')->withData($data);
})
->name('usr-timeline');

/* Settings */

Route::prefix("settings")->group(function() {
    Route::get('/account', function () {                                                                                                                                                                                                        
        return view("usr.settings.account");
    })->name("usr-setting-account");

    Route::get('/preferences', function () {
        return view("usr.settings.preferences");
    })->name("usr-setting-preferences");

    Route::get('/notifications', function () {
        return view("usr.settings.user-settings-notifications");
    })->name("usr-setting-noti");

    Route::get('/others', function () {
        return view("usr.settings.user-settings-others");
    })->name("usr-setting-others");

});

Route::get('payment', function() {
    return view('usr.payment.payment');
})->name('pay');

/* Advertising routing */
Route::prefix('ads')->group(function() {
    Route::get('/register', function () {                                                                                                                                                                                                        
        return view("logged-in.users.advertising.register");
    })->name("ads-register");

    Route::get('/register-success', function () {                                                                                                                                                                                                        
        return view("logged-in.users.advertising.success");
    })->name("ads-register-success");
});

/* Shop routing */
Route::prefix('shop')->group(function() {
    Route::get('/all', function () {
        $data = Shop::all();
        return view("usr.shop.all")->withData($data);
    })->name('all-shop');

    Route::get('/{shop_id}', function ($shop_id) {
        $shop_info = Shop::findShop($shop_id);
        $shop_items = Item::findItemOf($shop_id);
        return view("usr.shop.front")->with('info', $shop_info)->with('items', $shop_items);
    })->name('shop');

    Route::get('/{shop_id}/{item_id}', function ($shop_id, $item_id) {
        $shop_info = Shop::findShop($shop_id);
        $item_info = Item::findItem($item_id);
        return view("usr.shop.item-detail")->with('info', $shop_info)->with('item', $item_info);
    })->name('shop-item-detail');

    Route::get('/register', function () {                                                                                                                                                                                                        
        return view("logged-in.users.shop.register");
    })->name("shop-register");

    Route::get('/register-success', function () {                                                                                                                                                                                                        
        return view("logged-in.users.shop.success");
    })->name("shop-register-success");
});