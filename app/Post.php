<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    // Định nghĩa têm bảng nằm trong database để sử dụng cho phương thức all()
    protected $table = 'posts';

    public static function info() {
        return DB::table('posts')->leftJoin('users', 'posts.user_id', '=', 'users.user_id')->select('posts.*', 'users.user_name')->get();
    }

    public static function new() {
        /*
        DB::table('posts')->insert(
            []
        );
        */
    }

}
