<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Item extends Model
{
    protected $table = 'items';

    public static function findItemOf($shop_id) {
        return DB::table('items')->where('shop_id', $shop_id)->get();
    }

    public static function findItem($item_id) {
        return DB::table('items')->where('item_id', $item_id)->first();
    }
}
