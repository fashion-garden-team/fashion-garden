<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Shop extends Model
{
    protected $table = 'shops';

    public static function findShop($shop_id) {
        return DB::table('shops')->where('shop_id', $shop_id)->first();
    }
}
