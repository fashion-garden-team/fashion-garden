
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('items-slide', require('./components/usr/post/ItemsSlide.vue'));
Vue.component('pw-changer', require('./components/user-settings/PasswordChanger.vue'));
Vue.component('ads', require('./components/usr/timeline/Ads.vue'));
Vue.component('timeline-post', require('./components/usr/timeline/TimelinePost.vue'));
Vue.component('post-tool', require('./components/usr/timeline/PostTool.vue'));


const app = new Vue({
    el: '#app'
});
