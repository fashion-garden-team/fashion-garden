# Khung layout cơ bản:

```
[app-layout]
    <div id="app">
        [nav]
        <div class="container-fluid">
            [content]
        </div>
    </div>
[/app-layout]
```

# Thư mục:
pub: dành cho khách
usr: dành cho người dùng
