@extends("app-layout")

@section('title', "John Doe")

@section('head')

@endsection

@section('content')
    <div class="container">
        <div class="cover-img">
            <img src="http://via.placeholder.com/1600x900">
        </div>

        <div class="columns">
            <div class="column">

                <div class="columns is-mobile">
                    <div class="column">
                        <div class="profile-img">
                            <img src="http://via.placeholder.com/60x60">
                        </div>
                    </div>

                    <div class="column">
                        <h1>John Doe</h1>
                        <small>john.doe</small>
                    </div>
                </div>

                <div class="user-post">
                    <div class="box">
                        <span>John Doe</span>
                        <article class="media">
                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <small>31m ago</small>
                                        <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur sit amet
                                        massa fringilla egestas. Nullam condimentum luctus turpis. Lorem ipsum dolor sit
                                        amet, consectetur adipiscing elit. Aenean efficitur sit amet massa fringilla egestas.
                                        Nullam condimentum luctus turpis. Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit. Aenean efficitur sit amet massa fringilla egestas. Nullam condimentum luctus
                                        turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur
                                        sit amet massa fringilla egestas. Nullam condimentum luctus turpis.
                                    </p>
                                </div>
                                <nav class="level is-mobile">
                                    <div class="level-left">
                                        <a class="level-item">
                                            <span class="icon is-small">
                                                <i class="fas fa-reply"></i>
                                            </span>
                                        </a>
                                        <a class="level-item">
                                            <span class="icon is-small">
                                                <i class="fas fa-retweet"></i>
                                            </span>
                                        </a>
                                        <a class="level-item">
                                            <span class="icon is-small">
                                                <i class="fas fa-heart"></i>
                                            </span>
                                        </a>
                                    </div>
                                </nav>
                            </div>
                        </article>
                    </div>

                    <div class="box">
                        <span>John Doe</span>
                        <article class="media">
                            <div class="media-content">
                                <div class="content">
                                    <p>
                                        <small>1h ago</small>
                                        <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur sit amet
                                        massa fringilla egestas. Nullam condimentum luctus turpis. Lorem ipsum dolor sit
                                        amet, consectetur adipiscing elit. Aenean efficitur sit amet massa fringilla egestas.
                                        Nullam condimentum luctus turpis. Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit. Aenean efficitur sit amet massa fringilla egestas. Nullam condimentum luctus
                                        turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur
                                        sit amet massa fringilla egestas. Nullam condimentum luctus turpis.
                                    </p>
                                </div>
                                <nav class="level is-mobile">
                                    <div class="level-left">
                                        <a class="level-item">
                                            <span class="icon is-small">
                                                <i class="fas fa-reply"></i>
                                            </span>
                                        </a>
                                        <a class="level-item">
                                            <span class="icon is-small">
                                                <i class="fas fa-retweet"></i>
                                            </span>
                                        </a>
                                        <a class="level-item">
                                            <span class="icon is-small">
                                                <i class="fas fa-heart"></i>
                                            </span>
                                        </a>
                                    </div>
                                </nav>
                            </div>
                        </article>
                    </div>

                </div>

            </div>

            <div class="column">
                <h1 class="title">Information</h1>
                <br>
                <h2>Type:
                    <span>Fashionista</span>
                </h2>
                <h2>Age:
                    <span>23</span>
                </h2>
                <h2>Live at:
                    <span>U.S</span>
                </h2>
                <h2>Bio:
                    <span>Hello world!</span>
                </h2>

                <br>
                <h1 class="title">Images</h1>
                <div class="columns is-gapless">
                    <div class="column">
                        <figure class="image is-128x128">
                            <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                        </figure>
                    </div>
                    <div class="column">
                        <figure class="image is-128x128">
                            <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                        </figure>
                    </div>
                    <div class="column">
                        <figure class="image is-128x128">
                            <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                        </figure>
                    </div>
                </div>
                <div class="columns is-gapless">
                    <div class="column">
                        <figure class="image is-128x128">
                            <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                        </figure>
                    </div>
                    <div class="column">
                        <figure class="image is-128x128">
                            <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                        </figure>
                    </div>
                    <div class="column">
                        <figure class="image is-128x128">
                            <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                        </figure>
                    </div>
                </div>
                <div class="columns is-gapless">
                    <div class="column">
                        <figure class="image is-128x128">
                            <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                        </figure>
                    </div>
                    <div class="column">
                        <figure class="image is-128x128">
                            <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                        </figure>
                    </div>
                    <div class="column">
                        <figure class="image is-128x128">
                            <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                        </figure>
                    </div>
                </div>
                <br>
                <h1 class="title">Interested in</h1>
                <div class="tags">
                    <span class="tag is-light">D&G</span>
                    <span class="tag is-light">La Coste</span>
                    <span class="tag is-light">Canifa</span>
                    <span class="tag is-light">Urban</span>
                    <span class="tag is-light">Viet Tien</span>
                    <span class="tag is-light">7 a.m</span>
                    <span class="tag is-light">May10</span>
                </div>
            </div>

        </div>
    </div>
@endsection