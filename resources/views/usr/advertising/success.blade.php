@extends("app-layout")
@section('title', 'Chiến dịch quảng cáo đã được thiết lập!')

@section('content')
<section class="hero is-fullheight">
    <div class="hero-body">
        <div class="container has-text-centered">
            <h1 class="title is-1">CHIẾN DỊCH QUẢNG CÁO ĐÃ ĐƯỢC THIẾT LẬP</h1>
            <h2 class="subtitle">Bây giờ, bạn có thể <a href="#">theo dõi</a> dữ liệu thống kê.!</h2>
        </div>
    </div>
</section>
@endsection