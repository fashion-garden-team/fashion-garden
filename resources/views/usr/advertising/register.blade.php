@extends('app-layout')
@section('title', 'Đăng kí quảng cáo')
@section('head')
<link rel="stylesheet" href="{{ URL::asset('css/ads-register.css') }}">
@endsection

@section('content')
<div id="register" class="columns is-centered">
    <div class="column is-half has-text-centered">
        <h1 class="title">Nhập thông tin cho mẫu quảng cáo của bạn</h1>
        <input type="text" placeholder="Tiêu đề">
        <br>
        <input type="text" placeholder="Mô tả ngắn">
        <br>
        <input type="text" placeholder="Mô tả chi tiết">
        <br>
        <p class="title is-5">Chọn gói dịch vụ bạn cần:</p>
        <label>Chiến dịch ngắn (1-3 ngày)
            <input type="checkbox">
        </label>
        <br>
        <label>Chiến dịch trung bình (1 tuần)
            <input type="checkbox">
        </label>
        <br>
        <label>Chiến dịch dài (1 tháng)
            <input type="checkbox">
        </label>
        <br>
        <a id="submit-btn" href="{{ route('ads-register-success')}}">Xác nhận</a>
    </div>
</div>


@endsection