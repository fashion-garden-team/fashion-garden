@push('custom-styles')
<link rel="stylesheet" href="{{ asset('css/timeline-nav.css') }}">
@endpush


<nav id="timeline-nav">
    <div class="container">
        
        <div class="timeline-nav-wrapper">
            <div class="timeline-nav-left">
                <a href="{{ route('home') }}"><b>Fashion Garden</b></a>
            </div>
            <div class="timeline-nav-right">
            <span class="icon">
        <i class="far fa-compass"></i>
        </span>

        <span class="icon">
            <i class="far fa-user"></i>
        </span>

        <span class="icon">
        <i class="far fa-paper-plane"></i>
        </span>

        <span class="icon">
        <i class="far fa-bell"></i>
        </span>
            </div>
        </div>
        
        
    </div>

</nav>