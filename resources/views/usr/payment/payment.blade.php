@extends('app-layout')

@section('title', "Fashion Garden")

@section('head')
<style>
    input {
        font-size: 1em;
        margin-top: 0.5em;
    }
    select {
        font-size: 1em;
        margin-top: 0.5em;
    }
    .electrical-wallet-wrapper {
        display: grid;
        grid-template-columns: 50% 50%;
        grid-gap: 30px;
    }
</style>
@endsection

@section('nav')

@include('usr.parts.nav')

@endsection

@section('content')
<div class="container">
    <h1 class="title">Thanh toán</h1>
    <p>Chọn phương án thanh toán:</p>
    <div class="columns">
        <div class="column">
            <p class="title is-4">Thanh toán khi nhận hàng</p>
            <input type="text" placeholder="Số nhà, đường, ngõ">
            <br>
            <select>
                <option value="volvo">Nguyễn Du</option>
                <option value="opel">Tân Mai</option>
                <option value="audi">Hàng Bài</option>
            </select>
            <br>
            <select>
                <option value="volvo">Hai Bà Trưng</option>
                <option value="saab">Hoàn Kiếm</option>
                <option value="opel">Long Biên</option>
                <option value="audi">Hoàng Mai</option>
            </select>
            <br>
            <select>
                <option value="volvo">Hà Nội</option>
            </select>
            <br>
            <button class="button is-dark is-centered">Xác nhận</button>
        </div>
        <div class="column">
            <p class="title is-4">Thẻ ngân hàng</p>
            <input type="text" placeholder="Tên">
            <br>
            <input type="text" placeholder="Mã số thẻ">
            <br>
            <select>
                <option value="volvo">1</option>
                <option value="opel">2</option>
                <option value="audi">3</option>
                <option value="volvo">4</option>
                <option value="opel">5</option>
                <option value="audi">6</option>
                <option value="volvo">7</option>
                <option value="opel">8</option>
                <option value="audi">9</option>
                <option value="volvo">10</option>
                <option value="opel">11</option>
                <option value="audi">12</option>
            </select>
            <br>
            <select>
                <option value="volvo">2018</option>
                <option value="opel">2019</option>
                <option value="audi">2020</option>
                <option value="volvo">2021</option>
            </select>
            <br>
            <input type="text" placeholder="CVV">
            <br>
            <button class="button is-dark is-centered">Xác nhận</button>
        </div>
        <div class="column">
            <p class="title is-4">Ví điện tử</p>
            <div class="electrical-wallet-wrapper">
            <figure class="image"><img src="https://payment.momo.vn/images/momo.jpg" alt=""></figure>
            <figure class="image"><img src="https://thebank.vn/static/6/1135/714/90/2018/08/09/thebank_thanhtoannhanhchongvoividientunganluong_1533790709.png" alt=""></figure>
            <figure class="image"><img src="https://i2.wp.com/vn.bo2d.com/wp-content/uploads/2018/07/huong-dan-rut-tien-olymp-trade-ve-vi-dien-tu-bao-kim.jpg?fit=700%2C400&ssl=1" alt=""></figure>
            </div>
            
            <button class="button is-dark is-centered">Tiếp tục</button>
        </div>
    </div>

</div>

@endsection