<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
        <link rel="stylesheet" href="{{ URL::asset('css/nav.css') }}">
        @yield('head')
    </head>
    <body>
        <div id="app">
            @yield('nav')
            <div class="container">
                <div class="columns is-centered">
                    <div class="column is-three-quarters">
                        <div class="columns">
                            <div class="column is-3">
                                <a href="{{ route('user-settings-account') }}">
                                    <p>Account</p>
                                    <p>Your infomation, privacy</p>
                                </a>
                                <a href="{{ route('user-settings-preferences') }}">
                                    <p>Preferences</p>
                                    <p>Customize your experience</p>
                                </a>
                                <a href="{{ route('user-settings-notifications') }}">
                                    <p>Notifications</p>
                                    <p>Choose what you will see</p>
                                </a>
                                <a href="{{ route('user-settings-others') }}">
                                    <p>Other</p>
                                    <p>You find all here</p>
                                </a>
                            </div>
                            <div class="column" style="background: #DFDFDF;">
                                @yield('content')
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        <script defer src="https://use.fontawesome.com/releases/v5.0.0/js/all.js"></script>
        <script src="{{ URL::asset('js/app.js') }}"></script>
        <script src="{{ URL::asset('js/ui/tablet-down-nav.js') }}"></script>
    </body>