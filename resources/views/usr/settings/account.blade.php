@extends('logged-in.users.settings.settings-layout')

@section('title', "Account Settings")

@section('head')
    
@endsection

@section('nav')
@include('logged-in.parts.nav')
@endsection

@section('content')

<div class="columns">
    <div class="column is-one-quarter">
        <p>Name</p>
    </div>
    <div class="column">
        <input type="text">
    </div>
</div>
<div class="columns">
    <div class="column is-one-quarter">
        <p>Username</p>
    </div>
    <div class="column">
        <input type="text">
    </div>
</div>
<pw-changer></pw-changer>
<div class="columns">
    <div class="column is-one-quarter">
        <p>Email</p>
    </div>
    <div class="column">
        <input type="text">
    </div>
</div>
<div class="columns">
    <div class="column is-one-quarter">
        <p>Who can send you a friend request?</p>
    </div>
    <div class="column">
        <select name="" id="">
            <option value="">Anyone</option>
            <option value="">Friends of friends</option>
            <option value="">No one</option>
        </select>
    </div>
</div>
<div class="columns">
    <div class="column is-one-quarter">
        <p>Who can see your post?</p>
    </div>
    <div class="column">
        <select name="" id="">
            <option value="">Anyone</option>
            <option value="">Friends of friends</option>
            <option value="">Friends</option>
        </select>
    </div>
</div>

@endsection