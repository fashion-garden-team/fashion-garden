@extends('app-layout')

@section('title', "Fashion Garden")

@section('head')
<style>
    input {
        font-size: 1em;
        font-weight: bold;
        text-align: center;
    }
</style>
@endsection

@section('nav')

@include('usr.parts.nav')

@endsection

@section('content')
<div class="container">
<br>
<div class="columns">
    <div class="column">
    <img src="{{ $item->img_url}}">
    </div>
    <div class="column">
    <p class="title is-3">{{ $item->item_name }}</p>
<p class="subtitle is-4">{{ $item->item_price }}đ</p>
<p>{{ $item->description }}</p>
<p>Số lượng: <button><span class="icon"><i class="fas fa-minus"></i></span></button><input type="text" value="1"><button><span class="icon"><i class="fas fa-plus"></i></span></button></p>
<br>
<a class="button is-dark" href="{{ route('pay') }}"><span class="icon"><i class="fas fa-shopping-cart"></i></span>&nbsp;Thêm vào giỏ hàng</a>
<a class="button is-danger"><span class="icon"><i class="fas fa-heart"></i></span>&nbsp;Yêu thích</a>
    </div>
</div>
</div>


@endsection