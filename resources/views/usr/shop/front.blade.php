@extends('app-layout')

@section('title', "Fashion Garden - {{ $info->shop_name }}")

@section('head')
<style>
    .shop-info-header {
        padding: 1em 1em 1em 0;

    }
    .shop-info-header::after {
        display: block;
        content: '';
        margin-top: 0.5em;
        width: 100%;
        height: 3px;
        background: #218282;
    }
    .shop-items-wrapper {
        display: grid;
        grid-template-columns: 25% 25% 25% 25%;
    }
    .image img {
        max-height: 300px;
    }
</style>
@endsection

@section('nav')

@include('usr.parts.nav')

@endsection

@section('content')
<br>
<div class="container">
    <p class="title is-3">{{ $info->shop_name }}</p>
    <p class="subtitle is-4">Lượt thích: {{ $info->likes_count }}</p>

    <p class="shop-info-header title is-4">Nổi bật</p>
    <div class="shop-items-wrapper">
    <div>
        <figure class="image">
            <img src="{{ $items[0]->img_url }}" alt="">
        </figure>
        <a href="{{ route('shop-item-detail', ['shop_id' => $info->shop_id, 'item_id' => $items[0]->item_id]) }}">{{ $items[0]->item_name }}</a><br>
            <b>{{ $items[0]->item_price }}</b>
    </div>

    <div>
        <figure class="image">
            <img src="{{ $items[2]->img_url }}" alt="">
        </figure>
        <a>{{ $items[2]->item_name }}</a><br>
            <b>{{ $items[2]->item_price }}</b>
    </div>
    </div>

    <p class="shop-info-header title is-4">Mới</p>
    <div class="shop-items-wrapper">
    @foreach ($items as $item)

    <div>
    <figure class="image"><img src="{{ $item->img_url }}" alt=""></figure>
    <a href="{{ route('shop-item-detail', ['shop_id' => $info->shop_id, 'item_id' => $item->item_id]) }}">{{ $item->item_name}}</a><br>
    <b>{{ $item->item_price }}đ</b>
    </div>

    @endforeach
    </div>
    
    <p class="shop-info-header title is-4">Được khuyên dùng bởi shop</p>
    <div class="shop-items-wrapper">
    <div>
        <figure class="image">
            <img src="{{ $items[1]->img_url }}" alt="">
            <a href="{{ route('shop-item-detail', ['shop_id' => $info->shop_id, 'item_id' => $items[1]->item_id]) }}">{{ $items[1]->item_name }}</a><br>
            <b>{{ $items[1]->item_price }}</b>
        </figure>
    </div>
    </div>

</div>

@endsection