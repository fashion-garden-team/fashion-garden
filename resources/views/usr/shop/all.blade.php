@extends('app-layout')

@section('title', "Fashion Garden - Shop")

@section('head')
<style>
    .shops-list-wrapper {
        display: grid;
        grid-template-columns: 50% 50%;
    }
</style>
@endsection

@section('nav')

@include('usr.parts.nav')

@endsection

@section('content')

<div class="container">
    <p class="title is-3">Shop đang hoạt động</p>
    <div class="shops-list-wrapper">
        @foreach ($data as $shop)
        <div class="shop">
            <figure class="image">
                <img src="{{ $shop->img_url }}" alt="{{ $shop->shop_name }}">
            </figure>
            
            <a class="title is-4" href="{{ route('shop', ['shop_id' => $shop->shop_id]) }}">{{ $shop->shop_name}}</a>
            <p>{{ $shop->likes_count }} lượt thích</p>
        </div>
        @endforeach
    </div>
    
</div>

@endsection