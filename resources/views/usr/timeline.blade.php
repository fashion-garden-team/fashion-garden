@extends('app-layout')

@section('title', "Fashion Garden")

@section('head')
@endsection

@section('nav')

@include('usr.parts.nav')

@endsection

@section('content')
<br>
<div class="container">
    <div class="columns">
        <div class="column">
            <post-tool></post-tool>
            <br>
            @foreach ($data as $post)
            <timeline-post
                usr-name="{{ $post->user_name }}"
                content="{{ $post->post_content }}"
                :enjoys-count="{{ $post->post_enjoys_count }}"
                :shares-count="{{ $post->post_shares_count }}">
            </timeline-post>

            <items-slide></items-slide>
            @endforeach
        </div>

        <div class="column">
            <ads ad-name="Áo dài"
                ad-description="Một bộ cánh tuyệt đẹp với màu đỏ chủ đạo."
                image-url="{{ asset('img/items/aodai.png') }}">
            </ads>
        </div>
    </div>
</div>


@endsection