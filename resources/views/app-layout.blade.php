<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('css/bulma.css') }}">
    @yield('head')
    @stack('custom-styles')
    
    <script defer src="{{ asset('js/all.min.js') }}"></script>
    <script defer src="{{ asset('js/app.js') }}"></script>
    @stack('custom-scripts')
</head>

<body>
    <div id="app">
        @yield('nav')
        
        <div class="container-fluid">
            @yield('content')
        </div>
        
    </div>
</body>

</html>