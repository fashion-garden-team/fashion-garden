@extends("app-layout")

@section('title', 'Login - Fashion Garden')

@section('head')
    <link rel="stylesheet" href="{{ URL::asset('css/login-page.css') }}">
@endsection

@section('content')
    <div class="columns is-centered">
        <div class="column is-half is-narrow">
            <div id="login" class="hero is-fullheight">
                <div class="hero-body">
                    <div class="container">
                        <form>
                            <h1 class="title">Đăng nhập</h1>
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" placeholder="Tên người dùng hoặc email">
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="password" class="input" placeholder="Mật khẩu">
                                </div>
                            </div>
                            <a id="fg-login-btn" href="{{ route('usr-timeline') }}">Đăng Nhập</a>
                            <br>
                            <br>
                            <p>
                                Chưa có tài khoản?
                                <a href="">Đăng ký</a>
                                <br>
                                <a href="{{ route('home') }}">
                                    < Về trang chủ</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection