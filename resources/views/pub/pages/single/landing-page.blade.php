@extends("app-layout") @section('title', 'Wellcome to Fashion Garden!') @section('head')
<link rel="stylesheet" href="{{ URL::asset('css/landing-page.css') }}"> @endsection @section('content')

<div id="content" class="container-fluid" style="background: url('img/bg/bg-2.jpg') center; background-position: cover;">
    <nav class="desktop is-hidden-touch">
        <h1 class="home">Fashion Garden</h1>
        <ul>
            <li>
                <a href="#">Xu Hướng</a>
            </li>
            <li>
                <a href="#">Tin Mới</a> 
            </li>
            <li>
                <a href="#">Sự Kiện</a>
            </li>
            <li>
                <a href="#">Tài Khoản</a>
            </li>
        </ul>
    </nav>

    <nav class="mobile is-hidden-tablet">
        <h1 class="home">Fashion Garden</h1>
        <span class="icon bars">
            <i class="fas fa-bars"></i>
        </span>
    </nav>

    <header>
        <div>
            <h1 class="section-header">Thời trang để chia sẻ</h1>
            <p class="header-des">Fashion Garden là một mạng xã hội được tạo ra để bạn chia sẻ về thời trang, kết nối với những con
                người có cùng sở thích.
            </p>
            <br>
            <div>
                <a id="story-link" href="#">
                    <span class="icon">
                        <i class="fas fa-question-circle"></i>
                    </span>
                    <i>Câu chuyện đằng sau dự án</i>
                </a>
            </div>
            <br>
            <span>
                <a id="fg-get-app" class="landing-btn" href="#sec-3">TẢI APP</a>
                <a id="fg-sign-in" class="landing-btn" href="{{ route('login') }}">ĐĂNG NHẬP</a>
            </span>
        </div>

    </header>
</div>
<!-- Header -->
<section id="sec-1">
    <div class="container">
        <center>
            <h2 class="section-header">Cộng đồng dành cho thời trang</h2>
            <p class="des">Fashion Garden được tạo ra với mục tiêu trở thành một cộng đồng đành cho nhiều đối tượng khác nhau trong làng thời
                trang.</p>
        </center>
        <div class="columns is-gapless">
            <div class="column object">
                <h3>Tín đồ thời trang</h3>
                <p>Khám phá xu hướng thời trang, tìm cho mình một bộ cánh ưng ý.</p>
            </div>
            <div class="column object">
                <h3>Người mẫu</h3>
                <p>Diện những bộ trang phục thời thượng, "mốt" nhất đến với công chúng.</p>
            </div>
            <div class="column object">
                <h3>Nhà thiết kế / Thương hiệu</h3>
                <p>Giới thiệu, kinh doanh các sản phẩm thời trang, tổ chức show diễn.</p>
            </div>
        </div>
    </div>
</section>
<section id="sec-2">
    <div class="container">
        <center>
            <h2 class="section-header">Sẵn sàng để khám phá</h2>
            <p class="des">Trải nghiệm chưa bao giờ tuyệt vời hơn!</p>

            <div class="tabs is-toggle is-fullwidth">
                <ul>
                    <li class="is-active">
                        <a>
                            <span class="icon is-small">
                                <i class="fas fa-image"></i>
                            </span>
                            <span>Giao diện</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <span class="icon is-small">
                                <i class="fas fa-music"></i>
                            </span>
                            <span>Tuỳ biến</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <span class="icon is-small">
                                <i class="fas fa-film"></i>
                            </span>
                            <span>Đăng tải</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <span class="icon is-small">
                                <i class="fas fa-file-alt"></i>
                            </span>
                            <span>Trò chuyện</span>
                        </a>
                    </li>
                </ul>
            </div>
        </center>

        <div class="tab-contents">
            <div id="first-tab" class="tab-content">
                <p>Dễ dàng tìm kiếm nội dung bạn muốn theo dõi.</p>
                <img src="{{ URL::asset('img/landing-page/img-ui.png') }}" alt="">
            </div>
            <div id="second-tab" class="tab-content" style="display: none;">
                <p>Không thích giao diện hiện tại? Hãy tự tạo cho bạn một giao diện phù hợp!</p>
                <img src="" alt="">
            </div>
            <div id="third-tab" class="tab-content" style="display: none;">
                <p>Trạng thái, ảnh, video, ... tất cả đang chờ bạn chia sẻ.</p>
                <img src="" alt="">
            </div>
            <div id="fourth-tab" class="tab-content" style="display: none;">
                <p>Từ góc phải màn hình, chọn một người bất kì trong danh sách và bắt đầu tán gẫu.</p>
                <img src="" alt="">
            </div>
        </div>
    </div>
</section>

<section id="sec-3">
    <div class="columns">
        <div class="column is-5">
            <figure class="image">
                <img src="{{ URL::asset('img/landing-page/iphone.jpeg') }}" alt="Download App">
            </figure>
        </div>
        <div class="column">
            <h2 class="section-header">Tải ứng dụng</h1>
                <p class="des">Không chỉ xuất hiện trên web, Fashion Garden đã có sẵn ứng dụng để bạn sử dụng trên các thiết bị smartphone.</p>
                <a class="download">
                    <i class="fab fa-apple"></i>
                    <span>App Store</span>
                </a>
                <br>
                <a class="download">
                    <i class="fab fa-android"></i>
                    <span>Google Play Store</span>
                </a>
                <br>
                <a class="download" href="#">
                    <i class="fab fa-windows"></i>
                    <span>Windows Store</span>
                </a>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="columns">
            <div class="column is-6">
                <h2>
                    <b>THÔNG TIN THÊM</b>
                </h2>
                <ul>
                    <li>
                        <a href="">Fashion Garden Team</a>
                    </li>
                    <li>
                        <a href="">Quy định & điều khoản</a>
                    </li>
                    <li>
                        <a href="">Giấy phép</a>
                    </li>
                </ul>
            </div>
            <div class="column is-6">
                <h2>
                    <b>HỢP TÁC</b>
                </h2>
                <ul>
                    <li>
                        <a href="">Đầu tư</a>
                    </li>
                    <li>
                        <a href="">Tổ chức sự kiện</a>
                    </li>
                    <li>
                        <a href="">Thương hiệu</a>
                    </li>
                    <li>
                        <a href="">Quảng cáo</a>
                    </li>
                </ul>
            </div>
        </div>
        <center>
            <p>
                <b>&copy; 2017 - Fashion Garden Team</b>
            </p>
        </center>
    </div>
</footer>


@endsection